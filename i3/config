set $mod Mod4
font xft:URWGothic-Book 14

new_window pixel 1
new_float normal

hide_edge_borders none

# Polybar and conky
exec_always --no-startup-id $HOME/.config/polybar/start.py &
exec conky -q -c $HOME/.conky/Sidebar-Conky_Rc&

# Wallpaper
exec_always --no-startup-id  bash $HOME/.fehbg

# No sleep
exec_always --no-startup-id  xset s off
exec_always --no-startup-id  xset -dpms
exec_always --no-startup-id  xset s noblank

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock-fancy --nofork

# NetworkManager is the most popular way to manage wireless networks on Linux,
# and nm-applet is a desktop environment-independent system tray GUI for it.
exec --no-startup-id nm-applet

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# -------------------------- SHORTCUTS --------------------------

bindsym --release Print exec scrot 'screenshot_%Y%m%d_%H%M%S.png' -e 'mkdir -p ~/Pictures/screenshots && mv $f ~/Pictures/screenshots && xclip -selection clipboard -t image/png -i ~/Pictures/screenshots/`ls -1 -t ~/Pictures/screenshots | head -1`' # All screens

bindsym --release Ctrl+Print exec scrot -s 'screenshot_%Y%m%d_%H%M%S.png' -e 'mkdir -p ~/Pictures/screenshots && mv $f ~/Pictures/screenshots && xclip -selection clipboard -t image/png -i ~/Pictures/screenshots/`ls -1 -t ~/Pictures/screenshots | head -1`' # Area selection


# use Mouse+$mod to drag floating windows to their wanted position
floating_modifier Mod4

# Ressurect
set $i3_resurrect i3-resurrect

bindsym $mod+s exec "$i3_resurrect save -w 1 ; $i3_resurrect save -w 2 ; $i3_resurrect save -w 3 ; $i3_resurrect save -w 4 ; $i3_resurrect save -w 5 ; $i3_resurrect save -w 6 ; $i3_resurrect save -w 7 ; $i3_resurrect save -w 8 ; $i3_resurrect save -w 9 ; $i3_resurrect save -w 10 ; "

bindsym $mod+n exec "$i3_resurrect restore -w 1 ;$i3_resurrect restore -w 2 ;$i3_resurrect restore -w 3 ;$i3_resurrect restore -w 4 ;$i3_resurrect restore -w 5 ;$i3_resurrect restore -w 6 ;$i3_resurrect restore -w 7 ;$i3_resurrect restore -w 8 ;$i3_resurrect restore -w 9 ;$i3_resurrect restore -w 10"


# start a terminal
bindsym $mod+Return exec i3-sensible-terminal


# Lock screen
bindsym $mod+l exec i3lock-fancy

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
# bindsym $mod+d exec dmenu_run
bindsym $mod+d exec --no-startup-id rofi -show run

# change focus
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
# bindsym $mod+s layout stacking
# bindsym $mod+w layout tabbed
# bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym $mod+minus scratchpad show

workspace_auto_back_and_forth yes

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1"
set $ws2 "2"
set $ws3 "3"
set $ws4 "4"
set $ws5 "5"
set $ws6 "6"
set $ws7 "7"
set $ws8 "8"
set $ws9 "9"
set $ws10 "10"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym Left        resize shrink width 5 px or 5 ppt
        bindsym Down        resize grow height 5 px or 5 ppt
        bindsym Up          resize shrink height 5 px or 5 ppt
        bindsym Right       resize grow width 5 px or 5 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# ---------------------------------- BAR ----------------------------------

# # Start i3bar to display a workspace bar (plus the system information i3status
# # finds out, if available)
# bar {
#   status_command i3status
# 	position top
#   mode dock
# }
#
# # Autostart applications
# exec --no-startup-id nitrogen --restore; sleep 1; compton -b
# exec --no-startup-id nm-applet
# # exec --no-startup-id xfce4-power-manager
# exec --no-startup-id pamac-tray
# exec --no-startup-id clipit
# exec_always --no-startup-id ff-theme-util
# exec_always --no-startup-id fix_xcursor
# # exec_always --no-startup-id $HOME/.config/polybar/i3wmthemer_bar_launch.sh

# Theme colors
client.focused          #272827 #272827 #657b83 #272827 #272827
client.focused_inactive #272827 #272827 #657b83 #272827 #272827
client.unfocused        #272827 #272827 #657b83 #272827 #272827
client.urgent           #272827 #272827 #657b83 #272827 #272827
client.placeholder      #272827 #272827 #657b83 #272827 #272827

client.background       #272827

# Gaps
# gaps inner 5
# gaps outer -4
#
# smart_gaps on
