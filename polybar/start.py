#!/usr/bin/env python3

import hashlib
import os
import subprocess
import sys
from typing import List, Optional


def get_all_monitors():
    res: bytes = subprocess.check_output(['polybar', '-m'])
    checksum = hashlib.md5(b';'.join(sorted(res.split(b'\n')))).hexdigest()

    monitors = sorted([
        {'name': i.split(':')[0].strip(), 'primary': '(primary)' in i}
        for i in res.decode('utf-8').split('\n')
        if i.strip()
    ], key=lambda v: v['name'])
    return monitors, checksum


def stop_polybar():
    os.system('killall -q polybar')
    os.system(f'while pgrep -x polybar >/dev/null; do sleep 1; done')


def start_polybar(monitors: List[dict], checksum: Optional[str] = None):
    stop_polybar()
    if checksum:
        screen_filename = f'~/.config/polybar/screens/{checksum}.sh'
        if os.path.isfile(screen_filename):
            os.system(f'sh {screen_filename}')
    for monitor in monitors:
        if monitor['primary']:
            os.system(f'MONITOR={monitor["name"]} polybar -r main -l info &')
        else:
            os.system(f'MONITOR={monitor["name"]} polybar -r secondary -l info &')


if __name__ == '__main__':
    all_monitors, setup_checksum = get_all_monitors()
    if len(sys.argv) == 2 and sys.argv[1] == 'checksum':
        print(setup_checksum, end='')
    elif len(sys.argv) == 1:
        start_polybar(all_monitors, setup_checksum)
