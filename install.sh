#!/usr/bin/env bash

TG_USR="rui"
TG_HOME="/home/$TG_USR"

install_i3 () {
  apt-get install -y \
    i3 i3lock i3lock-fancy python3 python3-dev python3-pip scrot xclip
  pip3 install --upgrade i3-resurrect
  mkdir -p $TG_HOME/.config/i3
  cp i3/config $TG_HOME/.config/i3/config
  chown -R $TG_USR.$TG_USR $TG_HOME/.config/i3
}

install_feh () {
  apt-get install -y feh
  mkdir -p $TG_HOME/.config/feh
  cp feh/bg.png $TG_HOME/.config/feh/bg.png
  # Start bg on startup
  # echo "#!/usr/bin/env bash" > $TG_HOME/.fehbg
  # echo "" >> $TG_HOME/.fehbg
  echo "feh --no-fehbg --bg-center --geometry=-0-0 $TG_HOME/.config/feh/bg.png" >> $TG_HOME/.fehbg
  echo "" >> $TG_HOME/.fehbg
  chown -R $TG_USR.$TG_USR $TG_HOME/.fehbg
}

install_bashrc () {
  cat bashrc/.bashrc >> $TG_HOME/.bashrc
  chown -R $TG_USR.$TG_USR $TG_HOME/.bashrc
}

install_rofi() {
  apt-get install -y rofi
  # Copy and configure theme
  mkdir -p $TG_HOME/.config/rofi
  cp rofi/theme.rasi $TG_HOME/.config/rofi/theme.rasi
  echo "rofi.theme: $TG_HOME/.config/rofi/theme.rasi" > $TG_HOME/.config/rofi/config.rasi
  chown -R $TG_USR.$TG_USR $TG_HOME/.config/rofi
}

install_fonts() {
  mkdir -p $TG_HOME/.fonts
  cp fonts/* $TG_HOME/.fonts/
  chown -R $TG_USR.$TG_USR $TG_HOME/.fonts
  fc-cache -f
  su $TG_USR fc-cache -f
}

install_polybar() {
  pushd .
  # Install
  apt-get install -y \
    cmake cmake-data libcairo2-dev libxcb1-dev libxcb-ewmh-dev \
    libxcb-icccm4-dev libxcb-image0-dev libxcb-randr0-dev \
    libxcb-util0-dev libxcb-xkb-dev pkg-config python3-xcbgen \
    xcb-proto libxcb-xrm-dev i3-wm libasound2-dev libmpdclient-dev \
    libiw-dev libcurl4-openssl-dev libpulse-dev \
    libxcb-composite0-dev xcb libxcb-ewmh2 git python3 libjsoncpp-dev
  cd /tmp
  git clone https://github.com/jaagr/polybar.git polybar
  cd polybar
  git checkout 3.5.7
  ./build.sh --all-features --auto
  cd build
  make userconfig
  popd
  # Configure
  mkdir -p $TG_HOME/.config/polybar/screens
  cp polybar/config $TG_HOME/.config/polybar
  cp polybar/start.py $TG_HOME/.config/polybar
  chown -R $TG_USR.$TG_USR $TG_HOME/.config/polybar
}

install_terminator () {
  apt-get install -y terminator
  mkdir -p $TG_HOME/.config/terminator
  cp terminator/config $TG_HOME/.config/terminator
  chown -R $TG_USR.$TG_USR $TG_HOME/.config/terminator
}

install_atom() {
  pushd .
  apt-get -y install atom
  cd /tmp
  git clone https://github.com/kmfk/atom-gruvbox-dark atom-gruvbox
  cd atom-gruvbox
}


if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

install_i3
install_rofi
install_fonts
install_polybar
install_terminator
install_feh
install_bashrc

echo "Done, now restart. If fonts are looking weird, reinstall them"
echo "from $TG_HOME/.fonts"
