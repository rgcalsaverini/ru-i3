Open the `install.sh` script, replace with your username, and run it to install.

If things look weird:
  - Reinstall the fonts on ~/.fonts
  - Make sure your user has the right permissions to ~/.config

Post installation steps:

This setup can automatically recognize which of your pre-configured monitor setups you have,
for that to work, create a layout shell script file for each of them, and put it on the
right folder:

1. Using `arandr`, organize your monitors to your liking, then save the scrip to `tmp.sh`
2. Get the checksum ID of your layout with `~/.config/polybar/start.py checksum`
3. Rename your script to `<checksum>.sh` and move it to `~/.config/polybar/screens`,
4. Done, now when this exact checksum is encountered again, it will load the right file
Ex:
```
# Create your screen layout with arandr
mv tmp.sh ~/.config/polybar/screens/$(~/.config/polybar/start.py checksum).sh
```

On atom:
  - install gruvbox theme
  - add minimap, highlight-selected, pigments
