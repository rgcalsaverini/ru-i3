#!/usr/bin/env python3

import subprocess
import re
import json
import sys
import time
import os

_core_temp_re = re.compile(r'Core [0-9]+:\s*\+([0-9.]+)°C')

_CACHE_PATH = '/tmp/sensor_data'


def save(f):
    def inner(*args, **kwargs):
        res = f(*args, **kwargs)
        with open(_CACHE_PATH, 'w') as fp:
            json.dump(res, fp)
        return res

    return inner


def restore():
    if os.path.isfile(_CACHE_PATH):
        with open(_CACHE_PATH, 'r') as fp:
            return json.load(fp)
    return {}


def num_cores():
    process = subprocess.Popen(['mpstat', '-P', 'ALL', '-o', 'JSON'], stdout=subprocess.PIPE)
    return json.loads(process.stdout.read().decode('utf-8'))['sysstat']['hosts'][0]['number-of-cpus']


def get_average_cpu_temperature(duration=0.1):
    measurements = list()
    start = time.time()
    while time.time() - start < duration:
        process = subprocess.Popen(['sensors'], stdout=subprocess.PIPE)
        res_lines = process.stdout.read().decode('utf-8').split('\n')
        core_temps = [_core_temp_re.findall(l) for l in res_lines]
        valid_temps = [float(l[0]) for l in core_temps if l]
        measurements.append(sum(valid_temps) / len(valid_temps))
    return '%.0f°C' % round(sum(measurements) / len(measurements), 0)


@save
def get_cpu_usage():
    measurements = dict()
    process = subprocess.Popen(['mpstat', '-P', 'ALL', '-o', 'JSON', '1', '1'], stdout=subprocess.PIPE)
    data = json.loads(process.stdout.read().decode('utf-8'))['sysstat']['hosts'][0]['statistics'][0]['cpu-load']
    for core in data:
        core_id = core['cpu']
        usage = 100 - core['idle']
        measurements[core_id] = '%.0f' % round(usage, 0)
    return measurements


arguments = sys.argv[1:]

if len(arguments) == 2:
    if arguments[0] == 'cpu-usage':
        print(get_cpu_usage()[arguments[1]])
    elif arguments[0] == 'restore-cpu-usage':
        print(restore()[arguments[1]])
    elif arguments[0] == 'cpu-temp' and arguments[1] == 'all':
        print(get_average_cpu_temperature())
elif len(arguments) == 1:
    if arguments[0] == 'cpu-count':
        print(num_cores())
